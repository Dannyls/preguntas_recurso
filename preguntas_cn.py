# ___Autor: Danny Lima___
# ___Email : danny.lima@unl.edu.ec___

"""Crear un programa en el cual se permita crear un Cuestionario que contenga al
 menos 3 preguntas de selección en base al modelo de clases codificado en el
 item 1. Presentar al final todo lo creado."""


class Cuestionario:
    asignar = ""
    nombre = ""
    pregunta = []

    def __init__(self,asignar, nombre):
        self.asignar = asignar
        self.nombre = nombre
        self.pregunta = []


class SeleccPregunta:
    numero = 0
    texto = ""
    opc1 = ""
    opc2 = ""
    opc3 = ""
    resp = ""

    def __init__(self, numero, texto):
        self.numero = numero
        self.texto = texto
        self.opc1 = ""
        self.opc2 = ""
        self.opc3 = ""
        self.resp = ""

    def __str__(self):
        return self.numero + "." + self.texto


pregunta_1 = Cuestionario("Pregunta 1:", "'El sistema nervioso se encarga de:'")
print(pregunta_1.asignar)
print(pregunta_1.nombre)
pregunta_1.pregunta = ["Seleccione la respuesta correcta:"]
print(pregunta_1.pregunta)
opc_1 = SeleccPregunta("1.", "Dirigir los procesos celulares")
print(opc_1.numero, opc_1.texto)
opc_2 = SeleccPregunta("2.", "Analizar la información que nos llega del exterior a través de los sentidos")
print(opc_2.numero, opc_2.texto)
opc_3 = SeleccPregunta("3.", "Pensar y reflexionar sobre los acontecimientos de la vida cotidiana")
print(opc_3.numero, opc_3.texto)


pregunta_2 = Cuestionario("Pregunta 2:", "El sistema nervioso está constituido por unas celulas llamadas:")
print(pregunta_2.asignar)
print(pregunta_2.nombre)
pregunta_2.pregunta = ["Seleccionar la respuesta correcta:"]
print(pregunta_2.pregunta)
cpo_1 = SeleccPregunta("1.", "'Neuronas'")
print(cpo_1.numero, cpo_1.texto)
cpo_2 = SeleccPregunta("2.", "'Mitocondrias'")
print(cpo_2.numero, cpo_2.texto)
cpo_3 = SeleccPregunta("3.", "'Nervios'")
print(cpo_3.numero, cpo_3.texto)


pregunta_3 = Cuestionario("Pregunta 3:", "")
print(pregunta_3.asignar)
print(pregunta_3.nombre)
pregunta_3.pregunta = ["En el sistema nervioso se distingen tres partes:"]
print(pregunta_3.pregunta)
pic_1 = SeleccPregunta("1.", "Encéfalo, medula espinal y nervios")
print(pic_1.numero, pic_1.texto)
pic_2 = SeleccPregunta("2.", "Encéfalo, neuronas y nervios")
print(pic_2.numero, pic_2.texto)
pic_3 = SeleccPregunta("3.", "Bulbo raquídeo, medula espinal y nervios")
print(pic_3.numero, pic_3.texto)
